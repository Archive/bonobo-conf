/**
 * bonobo-property-editor-list.h:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef _BONOBO_PROPERTY_EDITOR_LIST_H_
#define _BONOBO_PROPERTY_EDITOR_LIST_H_

#include "bonobo-property-editor.h"

BEGIN_GNOME_DECLS

#define BONOBO_PE_LIST_TYPE        (bonobo_pe_list_get_type ())
#define BONOBO_PE_LIST(o)          (GTK_CHECK_CAST ((o), BONOBO_PE_LIST_TYPE, BonoboPEList))
#define BONOBO_PE_LIST_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_PE_LIST_TYPE, BonoboPEListClass))
#define BONOBO_IS_PE_LIST(o)       (GTK_CHECK_TYPE ((o), BONOBO_PE_LIST_TYPE))
#define BONOBO_IS_PE_LIST_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_PE_LIST_TYPE))

typedef struct _BonoboPEListPrivate BonoboPEListPrivate;

typedef struct {
	BonoboPropertyEditor   base;
	BonoboPEListPrivate *priv;
} BonoboPEList;


typedef struct {
	BonoboPropertyEditorClass parent_class;
} BonoboPEListClass;

GtkType            
bonobo_pe_list_get_type               (void);

BonoboObject *
bonobo_peditor_sequence_new           ();

BonoboObject *
bonobo_peditor_array_new              ();

BonoboObject *
bonobo_peditor_sequence_construct     (GtkWidget *box, GtkWidget *sb);

END_GNOME_DECLS

#endif /* _BONOBO_PROPERTY_EDITOR_LIST_H_ */
