/**
 * bonobo-config-bag-property.h:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifndef __BONOBO_CONFIG_BAG_PROPERTY_H__
#define __BONOBO_CONFIG_BAG_PROPERTY_H__

#include <bonobo/bonobo-object.h>
#include <bonobo-config-bag.h>

BEGIN_GNOME_DECLS

BonoboTransient *bonobo_config_bag_property_transient (BonoboConfigBag *cb);

END_GNOME_DECLS

#endif /* ! __BONOBO_CONFIG_BAG_PROPERTY_H__ */
