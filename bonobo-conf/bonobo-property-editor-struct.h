/**
 * bonobo-property-editor-struct.h:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef _BONOBO_PROPERTY_EDITOR_STRUCT_H_
#define _BONOBO_PROPERTY_EDITOR_STRUCT_H_

#include "bonobo-property-editor.h"

BEGIN_GNOME_DECLS

#define BONOBO_PE_STRUCT_TYPE        (bonobo_pe_struct_get_type ())
#define BONOBO_PE_STRUCT(o)          (GTK_CHECK_CAST ((o), BONOBO_PE_STRUCT_TYPE, BonoboPEStruct))
#define BONOBO_PE_STRUCT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_PE_STRUCT_TYPE, BonoboPEStructClass))
#define BONOBO_IS_PE_STRUCT(o)       (GTK_CHECK_TYPE ((o), BONOBO_PE_STRUCT_TYPE))
#define BONOBO_IS_PE_STRUCT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_PE_STRUCT_TYPE))

typedef struct _BonoboPEStructPrivate BonoboPEStructPrivate;

typedef struct {
	BonoboPropertyEditor   base;
	BonoboPEStructPrivate *priv;
} BonoboPEStruct;


typedef struct {
	BonoboPropertyEditorClass parent_class;
} BonoboPEStructClass;

GtkType            
bonobo_pe_struct_get_type          (void);

BonoboObject *
bonobo_peditor_struct_new          ();

BonoboObject *
bonobo_peditor_struct_construct    (GtkWidget *widget);

END_GNOME_DECLS

#endif /* _BONOBO_PROPERTY_EDITOR_STRUCT_H_ */
