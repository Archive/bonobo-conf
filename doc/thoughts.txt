Configuration Monikers - Persistent PropertyBags
=================================================

Examples (moniker name - interface):
====================================

config:gnumeric/auto-save		IDL:/Bonobo/Property:1.0
config:gnumeric				IDL:/Bonobo/PropertyBag:1.0
config: 				IDL:/Bonobo/Config/Database:1.0

file:/tmp/test.tar#untar:/etc/config#config:gnumeric/autosave
file:/tmp/test.efs#efs:/config#config:fonts/default_font


Thoughts:
=========

Persistent PropertyBags are stored in some kind of container. You can address
them through a path. So the general syntax for an configuration moniker is:

[parent]config:			    if you want the container

[parent]config:path		    if you want the PropertyBag

[parent]config:path/property	    if you want the Property

Where we use GConf as the default container at the moment (if you omit the
parent) 

We can also use a Storage as an property bag container, where the property bags
are stored as Streams.


Notifications:
==============

config:path/property and file:/tmp/test.tar#untar#config:path/property are
different things! 

GConf Critique:
===============

The use of GConfValue makes things to difficult - we use CORBA:any instead.

There is a Database, Source, backend and a local/remote Engine interface: 
we don't need different names for the same interface, and use CORBA to 
access the Database Interface. 

GConf.idl needs a cleanup.

We use bonobo to make things simpler.

Why GError and not CORBA exceptions?


Miscellaneous:
==============

Although we can store CORBA:any it is still easier to store basic types
in some situation, i.e. when you have a structure which describes your
network setup:

struct {
       char *domainname;
       char *hostname;
       char *ipaddress;
};


Although you can save the whole structure as CORBA:any this is a bad
idea. Simply save them a separate values:

Network/domainname
Network/hostname
Network/ipaddress

XML Database format
===================

It should be easy to edit that file, and there must be a way to store
localised value:

<?xml version="1.0"?>
<bonobo-conf>
  <entry path="/test/int2" kind="int">
    <value>2</value>
  </entry>

  <entry path="/test/string2" kind="string">
    <value>a second string</value>
  </entry>

  <entry path="/another/string" kind="string">
    <value>another string</value>
    <value lang="de">deutsche ‹bersetzung</value>    
  </entry>

  <entry path="/complex/value" kind="enum">
    <type name="StorageType" repo_id="IDL:Bonobo/StorageType:1.0" sub_parts="2">
      <subnames>
        <name>STORAGE_TYPE_REGULAR</name>
        <name>STORAGE_TYPE_DIRECTORY</name>
      </subnames>
    </type>
    <value>1</value>
  </entry>

</bonobo-conf>
