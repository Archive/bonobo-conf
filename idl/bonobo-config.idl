/*
 * bonobo-config.idl: The Bonobo Configuration System
 *
 * Author:
 *	Dietmar Maurer (dietmar@ximian.com)
 */

#ifndef __BONOBO_CONFIG_IDL__
#define __BONOBO_CONFIG_IDL__

#include "bonobo-listener.idl"

module Bonobo {

	interface ConfigContext : Bonbo::Unknown {
		exception NotFound {};

		/*
		 * find an editor for tc
		 */
		PropertyEditor findEditor (in TypeCode tc)
			raises (NotFound);

		
	};

	interface ConfigDatabase;

	typedef sequence<string> KeyList;

	struct ConfigValue {
		string  key;
		any     value;
		boolean writeable;
	};

	struct ConfigEvent {
		ConfigDatabase database;
		ConfigValue    value;
	};

	typedef sequence<ConfigValue> ConfigList;

	/*
	 * aggregated with an event source
	 *
	 */
	interface ConfigDatabase : Unknown {

		void         setLocale  (in string locale);

		ConfigValue  getValue   (in string key);

		ConfigList   getValues  (in string dir);

		void         setValue   (in ConfigValue value);

		void         setValues  (in ConfigList values);

		string       getDoc     (in string key,
					 in boolean verbose);
 
		void         setDoc     (in string key,
					 in boolean verbose,
					 in string doc); 

		KeyList      listDirs   (in string dir);
		KeyList      listKeys   (in string dir);

		boolean      dirExists  (in string dir);
		
		void         remove     (in string path);

		boolean      writeable   ();
	  
		void         addDefaultDatabase (in ConfigDatabase ddb, 
						 in string filter_path,
						 in string append_path);
		void         sync       ();

	};

	interface ConfigDatabase : Unknown {

		boolean      writeable   ();

		PropertyBag  getPropertyBag  (in string dir);

		boolean      BagExists  (in string dir);
		
		KeyList      listDirs   (in string dir);
		KeyList      listKeys   (in string dir);

		void         remove     (in string path);

		void         sync       ();

		void         addDefaultDatabase (in ConfigDatabase ddb, 
						 in string filter_path,
						 in string append_path);

	};

};

#endif /* ! __BONOBO_CONFIG_IDL__ */
