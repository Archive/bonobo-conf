#include <stdlib.h>

#include <bonobo.h>

#include "bonobo-conf/bonobo-config-database.h"

#include "gconf/gconf-client.h"

static void
test_gconf ()
{
	Bonobo_ConfigDatabase db = NULL;
        CORBA_Environment  ev;
	char *key, *doc;

        CORBA_exception_init (&ev);

	db = bonobo_get_object ("gconf:", "Bonobo/ConfigDatabase", &ev);
	g_assert (!BONOBO_EX (&ev));
	g_assert (db != NULL);

	key = "/doc/short/apps/basic-gconf-app/bar";
	doc = bonobo_config_get_string (db, key, &ev);
	g_assert (!BONOBO_EX (&ev));

	printf ("FOUND: %s\n", doc);

	bonobo_object_release_unref (db, &ev);
	g_assert (!BONOBO_EX (&ev));
}

static gint
run_tests ()
{
	test_gconf ();

	gtk_main_quit ();

	return 0;
}

int
main (int argc, char **argv)
{
	CORBA_ORB orb;

	gnome_init ("test-gconf", "0.0", argc, argv);

	if ((orb = oaf_init (argc, argv)) == NULL)
		g_error ("Cannot init oaf");

	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error ("Cannot init bonobo");


	gtk_idle_add ((GtkFunction) run_tests, NULL);

	bonobo_main ();

	exit (0);
}
